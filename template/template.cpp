// template.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <cstdlib>
#include <cstdio>

using namespace std;

template <class T>
void Sort(T tab[], int iSize)
{
	for (int i = 0; i < iSize - 1; i++)
		for (int j = 0; j < iSize - 1; j++)
			if (tab[j] > tab[j + 1])
			{
				T tmp = tab[j];
				tab[j] = tab[j + 1];
				tab[j + 1] = tmp;
			}
}

template <class T>
void Display(T tab[], int iSize)
{
	for (int i = 0; i < iSize; i++)
	{
		cout << tab[i];
		cout << " ";
	}
}

int main()
{
	int tab[9] = { 2, 3, 1, 2, 5, 6, 7, 1, 2 };
	Sort<int>( tab, sizeof(tab) / sizeof(tab[0]));
	Display<int>(tab, sizeof(tab) / sizeof(tab[0]));
	cout << "\n";
	double tab2[5] = { 2.1, 5.7, 5.3, 10.1, 1.6 };
	Sort<double>(tab2, sizeof(tab2) / sizeof(tab2[0]));
	Display<double>(tab2, sizeof(tab2) / sizeof(tab2[0]));
	cout << "\n";
	char tab3[5] = { 'c', 'a', 'd', 'b', 'z' };
	Sort<char>(tab3, sizeof(tab3) / sizeof(tab3[0]));
	Display<char>(tab3, sizeof(tab3) / sizeof(tab3[0]));
    return 0;
}


#pragma once
#include "stdafx.h"
#include <iostream>
#include <cstdlib>
#include <cstdio>

using namespace std;

template<class T>
class CyclicBuffer
{
private:
	T *tab;
	T *buff;
	int iAddSeek;
	int iReadSeek;
	int iSize;
	bool bFull;
	int iCapacity;

public:
	CyclicBuffer(int _iSize);
	~CyclicBuffer();
	bool add(T _item);	
	T read();	
	bool isfull();
	int capacity();
};

template <class T>
CyclicBuffer<T>::CyclicBuffer(int _iSize)
{
	iSize = _iSize;
	tab = new T[_iSize];
	iAddSeek = 0;
	iReadSeek = 0;
	iCapacity = _iSize;
}

template <class T>
CyclicBuffer<T>::~CyclicBuffer()
{
	delete[] tab;
}

template <class T>
bool CyclicBuffer<T>::add(T _tItem)
{
	if (iCapacity > 0) {
	
		if (iAddSeek == iSize) {
			iAddSeek = 0;
		}

		tab[iAddSeek] = _tItem;
		iAddSeek++;
		iCapacity--;
		return 1;
	}
	else 
		return 0;
}

template <class T>
T CyclicBuffer<T>::read()
{
	T tTemp;
	if (iCapacity == iSize) {
		cout << "Buffer is empty" << endl;
		tTemp = tab[iReadSeek - 1];
	}
	else {
		if (iReadSeek == iSize) 
			iReadSeek = 0;

		tTemp = tab[iReadSeek];
		iCapacity++;
		iReadSeek++;
	}
	return tTemp;
}

template <class T>
bool CyclicBuffer<T>::isfull()
{
	return iCapacity == iSize;
}

template <class T>
int CyclicBuffer<T>::capacity()
{
	return iCapacity;
}
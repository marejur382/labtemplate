#pragma once
#include "stdafx.h"
#include <iostream>
#include <cstdlib>
#include <cstdio>

using namespace std;

template<class T>
class Stack
{
private:
	T *tab;
	T *buff;
	int iSize;

public:
	Stack();
	~Stack();
	void push(T _item);	//insert item into
	void pop();	//delete last item
	void empty();	//display items in table
	T top();	//sorting table
	int size();	//show table size
};

template <class T>
Stack<T>::Stack()
{
	tab = new T[0];
}

template <class T>
Stack<T>::~Stack()
{
	delete[] tab;
}

/**
Inserting new element to table

@param _item item to insert to table.
*/
template <class T>
void Stack<T>::push(T _item)
{
	buff = new T[this->iSize + 1];
	if (this->iSize == 0)
		buff[0] = _item;
	else {
		for (int i = 0; i < iSize; i++)
			buff[i] = this->tab[i];
		buff[iSize] = _item;
	}
	delete []tab;
	this->tab = buff;
	++this->iSize;
}

/**
Deleteing last element in the table
*/
template <class T>
void Stack<T>::pop()
{
	this->iSize -= 1;
	T *newTable = new T[this->iSize];
	for (int i = 0; i < this->iSize; ++i)
		newTable[i] = this->tab[i];
	delete[] this->tab;
	this->tab = newTable;
}

/**
@return 1 if stack is epmty
*/
template <class T>
void Stack<T>::empty()
{
	if (this->iSize == 0)
		return 1;
	else
		return 0;
}

/**
Printing size of table

@return return size of table
*/
template <class T>
int Stack<T>::size()
{
	return this->iSize;
}

template <class T>
T Stack<T>::top()
{
	return this->tab[iSize - 1];
}


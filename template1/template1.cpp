// template1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Stack.h"
#include "CyclicBuffer.h"
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <ctime>

using namespace std;

int main()
{
	Stack<int> intStack;
	intStack.push(1);
	intStack.push(2);
	intStack.push(3);
	cout << "top element: " << intStack.top() << endl;
	intStack.pop();
	cout << "top element: " << intStack.top() << endl;
	cout << "size: " << intStack.size() << endl;

	CyclicBuffer<int> intCycBuff(5);

	intCycBuff.add(5);
	intCycBuff.add(4);
	cout << intCycBuff.read() << endl;
	cout << intCycBuff.read() << endl;
	intCycBuff.add(3);
	intCycBuff.add(2);
	intCycBuff.add(1);
	intCycBuff.add(0);
	intCycBuff.add(1);
	intCycBuff.add(0);
	cout << intCycBuff.read() << endl;
	cout << intCycBuff.read() << endl;
	cout << intCycBuff.read() << endl;
	cout << intCycBuff.read() << endl;
	cout << intCycBuff.read() << endl;
	cout << intCycBuff.read() << endl;
	return 0;
}

